var express = require('express');
var bodyParser = require('body-parser');
var router = express.Router();
var pg = require('pg');

var app = express();

/* GET home page. */
router.get('/', function(req, res) {
  res.render('lifecalc', { title: 'Life Calculator!' });
});

app.use(bodyParser());




router.route('/lifecalc')

	.post(function(req, res) {
		var minutes = 1000*60;
    	var hours = minutes*60;
    	var days = hours*24;

    	var day = parseInt(req.body.day);
    	var month = parseInt(req.body.month);
    	var year = parseInt(req.body.year);

    	var birthdate = new Date(year, month-1, day);
    	var currentTime = new Date();

    	var diff_date = Math.round((currentTime.getTime() - birthdate.getTime())/days);

    	console.log('Days alive: ' + diff_date);

		pg.connect(process.env.DATABASE_URL, function(err, client, done) {
	    	client.query('CREATE TABLE IF NOT EXISTS life_calculator(name varchar(64), daysAlive integer, birthdate date)');
	    	client.query('CREATE TABLE IF NOT EXISTS bad_design(currentTime date)');
	    	client.query('INSERT INTO life_calculator(name, daysAlive, birthdate) values($1, $2, $3)', [req.body.name, diff_date, birthdate]);
	    	client.query('INSERT INTO bad_design(currentTime) values($1)', [currentTime]);
  		});

		console.log(req.body.name);
    	console.log(req.body.year);
    	console.log(req.body.month);
    	console.log(req.body.day);

    	res.render('lifecalcOutput', { title: 'Life Calculator!', name: req.body.name ,days: diff_date })
	})

	.get(function(req, res) {

		pg.connect(process.env.DATABASE_URL, function(err, client, done) {
			client.query('SELECT * FROM bad_design', function(err, result) {
     	 		done();
      			if (err)
      	 		{ 
      	 			client.query('CREATE TABLE IF NOT EXISTS bad_design(currentTime date)');

	    			client.query('INSERT INTO bad_design(currentTime) SELECT "currenttime" FROM life_calculator');
	    			client.query('ALTER TABLE life_calculator DROP COLUMN currentTime');
      	 		}
    		}); 	
    	});

		res.render('lifecalc', { title: 'Life Calculator!' })
	});


router.route('/history')

	.get(function(req, res) {
/*		var numRows
		var lifeCalcValues;
		var timestampValues;*/

		pg.connect(process.env.DATABASE_URL, function(err, client, done) {
/*			client.query('SELECT count(*) FROM life_calculator', function(err, result) {
     	 		done();
      			if (err)
      	 		{ console.error(err); response.send("Error " + err); }
     		 	else
      			{ numRows = result }
    		});*/

			client.query('SELECT * FROM life_calculator', function(err, result) {
     	 		done();
      			if (err)
      	 		{ console.error(err); response.send("Error " + err); }
     		 	else
      			{ res.render('history', { title: 'Life Calculator', dbValues: result.rows}) }
    		});

/*    		client.query('SELECT * FROM bad_design', function(err, result) {
     	 		done();
      			if (err)
      	 		{ console.error(err); response.send("Error " + err); }
     		 	else
      			{ timestampValues = result.rows }
    		});*/

    	});

    	
	});


module.exports = router;
